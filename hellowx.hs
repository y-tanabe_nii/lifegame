module Main where
import Graphics.UI.WX

import Control.Monad
import Data.Char
import Data.List
import qualified Data.Map as M

gxsize = 500
gysize = 500

-- Board is a list of the (x,y) coordinates of living cells.
type Board = [(Int, Int)]

data GuiRec = GuiRec { board :: Board
                     , xmin :: Int
                     , ymin :: Int
                     , xsize :: Int
                     , ysize :: Int
                     }
grUpdateBoard :: (Board -> Board) -> GuiRec -> GuiRec
grUpdateBoard f gr = gr{ board = f (board gr) }

main :: IO ()
main = start gui

gui :: IO ()
gui = do
    contents <- getContents 
    let brd = (fromBoardExp . lines) contents
    vRec <- varCreate (stdRec brd)
    f <- frameFixed [text := "Life Game"]
    p <- panel f [on paint := paintPanel vRec]
    t <- timer f [interval := 1000, enabled := False
                 ,on command := varUpdatePaint (grUpdateBoard nextGen) vRec p
                 ]
    set p [on (charKey '-') := set t [interval :~ \i -> i*2]
          ,on (charKey '+') := set t [interval :~ \i -> max 1 (i `div` 2)]
          ,on (charKey 's') := set t [enabled :~ not]        -- pause/restart
          ,on (charKey 'i') := varUpdatePaint grZoomin vRec p
          ,on (charKey 'o') := varUpdatePaint grZoomout vRec p
          ,on (charKey 'h') := varUpdatePaint (grShift  (-1)   0 ) vRec p
          ,on (charKey 'l') := varUpdatePaint (grShift    1    0 ) vRec p
          ,on (charKey 'j') := varUpdatePaint (grShift    0    1 ) vRec p
          ,on (charKey 'k') := varUpdatePaint (grShift    0  (-1)) vRec p
          ,on (charKey 'H') := varUpdatePaint (grShift (-10)   0 ) vRec p
          ,on (charKey 'L') := varUpdatePaint (grShift   10    0 ) vRec p
          ,on (charKey 'J') := varUpdatePaint (grShift    0   10 ) vRec p
          ,on (charKey 'K') := varUpdatePaint (grShift    0 (-10)) vRec p
          ,on (charKey (chr 12)) := varUpdatePaint (stdRec . board) vRec p -- Ctrl-L
          ,on (charKey (chr 17)) := close f -- Ctrl-Q
          ]
    let instructions = init . unlines $
                       [ "<s> to restart/pause      <+/-> to change the speed"
                       , "<h/j/k/l> to shift by one <H/J/K/L> to shift by ten"
                       , "<i/o> to zoom in/out"
                       , "<ctrl-L> to reposition    <ctrl-Q> to quit"
                       ]
    set f [layout := column 1 [ minsize (sz gxsize gysize) (widget p)
                              , label instructions ] ]
    return ()

  where

    paintPanel :: Var GuiRec -> DC () -> Rect -> IO ()
    paintPanel vRec dc rect = do
        set dc [brushColor := red, brushKind := BrushSolid]
        gr <- varGet vRec
        mapM_ (drawRect dc gr) (filter (onBoard gr) (board gr))
        -- putStrLn $ "xsize = " ++ (show (xsize gr)) ++ " ysize = " ++ (show (ysize gr))

    onBoard :: GuiRec -> (Int, Int) -> Bool
    onBoard gr (x,y) =    (xmin gr) <= x && x < (xmin gr) + (xsize gr)
                       && (ymin gr) <= y && y < (ymin gr) + (ysize gr)

    drawRect dc gr (x,y) = polygon dc (rectExp gr x y) []

    rectExp gr x y = map (gpoint gr) [(x,y), (x+1,y), (x+1,y+1), (x,y+1)]

    gpoint :: GuiRec -> (Int, Int) -> Point2 Int
    gpoint gr (x, y) = point (gx gr x) (gy gr y)

    gx :: GuiRec -> Int -> Int
    gx gr x = 10 + (gcell gr) * (x - (xmin gr))

    gy :: GuiRec -> Int -> Int
    gy gr y = 10 + (gcell gr) * (y - (ymin gr))

    gcell :: GuiRec -> Int
    gcell gr = min (div (gxsize - 20) (xsize gr)) (div (gysize - 20) (ysize gr))

    varUpdatePaint :: (GuiRec -> GuiRec) -> Var GuiRec -> Panel () -> IO ()
    varUpdatePaint f vRec p = do
        varUpdate vRec f
        repaint p

    grZoom :: (Int -> Int -> Int) -> (Int -> Int) -> GuiRec -> GuiRec
    grZoom newmin newsize gr
        | newsize (xsize gr) <= gxsize - 20 && newsize (ysize gr) <= gysize - 20
                    = gr{ xmin = newmin (xmin gr) (xsize gr)
                        , xsize = newsize (xsize gr)
                        , ymin = newmin (ymin gr) (ysize gr)
                        , ysize = newsize (ysize gr)
                        }
        | otherwise = gr

    grZoomin :: GuiRec -> GuiRec
    grZoomin = grZoom (\ min size -> min + (div size 4)) (`div` 2)

    grZoomout :: GuiRec -> GuiRec
    grZoomout = grZoom (\ min size -> min - (div size 2)) (* 2)

    grShift :: Int -> Int -> GuiRec -> GuiRec
    grShift x y gr = 
        let newgr = gr{ xmin = (xmin gr) + x
                      , ymin = (ymin gr) + y
                      }
            (xs, ys) = unzip (board gr)
            xmin0 = minimum xs
            ymin0 = minimum ys
            xmax0 = maximum xs
            ymax0 = maximum ys
        in if    xmin0 - 11 <= (xmin newgr)
              && ymin0 - 11 <= (ymin newgr)
              && (xmin newgr) + (xsize newgr) <= xmax0 + 11
              && (ymin newgr) + (ysize newgr) <= ymax0 + 11
           then newgr else gr

    stdRec :: Board -> GuiRec
    stdRec b = let (xs, ys) = unzip b
                   xmin = minimum xs
                   ymin = minimum ys
                   xmax = maximum xs
                   ymax = maximum ys
               in GuiRec b (xmin - 3) (ymin - 3) 
                         (min (xmax - xmin + 7) (gxsize - 20))
                         (min (ymax - ymin + 7) (gysize - 20))

-- the next generation
nextGen :: Board -> Board
nextGen board = 
    (map fst . filter f . M.assocs . countM . concat . map neighbour) board
  where f (p, i) = i == 3 || (i == 2 && elem p board)

-- the list of the eight coordinates surrounding given coordinate
neighbour :: (Int, Int) -> [(Int, Int)]
neighbour (x,y) = tail [ (i,j) | i <- [x,x-1,x+1], j <- [y,y-1,y+1] ]

-- counts the occurences in a list.
-- e.g. countM "abac" == fromList [('a', 2), ('b', 1), ('c', 1)]
countM :: Ord k => [k] -> M.Map k Int
countM = foldr (M.alter g) M.empty
  where g Nothing  = Just 1
        g (Just i) = Just (i + 1)

-- Input:  ["...X.",
--          "..X.X"]
-- Output: [(3,0), (2,1), (4,1)]
fromBoardExp :: [String] -> Board
fromBoardExp ss = concat $ zipWith funcRow ss [0..]
  where
    -- funcRow "...X.X" 7 = [(3,7), (5,7)]
    funcRow :: String -> Int -> [(Int, Int)]
    funcRow s y = (map g . filter ((/= '.') . fst) . zip s) [0..]
      where g (_, x) = (x,y)

