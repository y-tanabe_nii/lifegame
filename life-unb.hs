{-
  Life game, unbounded version
-}

import Data.List
import qualified Data.Map as M

-- Board is a list of the (x,y) coordinates of living cells.
type Board = [(Int, Int)]

main :: IO ()
main = getContents >>= (mapM_ showBoard . iterate nextGen . fromBoardExp . lines)

-- the next generation
nextGen :: Board -> Board
nextGen board = 
    (map fst . filter f . M.assocs . countM . concat . map neighbour) board
  where f (p, i) = i == 3 || (i == 2 && elem p board)

-- the list of the eight coordinates surrounding given coordinate
neighbour :: (Int, Int) -> [(Int, Int)]
neighbour (x,y) = tail [ (i,j) | i <- [x,x-1,x+1], j <- [y,y-1,y+1] ]

-- counts the occurences in a list.
-- e.g. countM "abac" == fromList [('a', 2), ('b', 1), ('c', 1)]
countM :: Ord k => [k] -> M.Map k Int
countM = foldr (M.alter g) M.empty
  where g Nothing  = Just 1
        g (Just i) = Just (i + 1)

-- Input:  ["...X.",
--          "..X.X"]
-- Output: [(3,0), (2,1), (4,1)]
fromBoardExp :: [String] -> Board
fromBoardExp ss = concat $ zipWith funcRow ss [0..]
  where
    -- funcRow "...X.X" 7 = [(3,7), (5,7)]
    funcRow :: String -> Int -> [(Int, Int)]
    funcRow s y = (map g . filter ((/= '.') . fst) . zip s) [0..]
      where g (_, x) = (x,y)

showBoard :: Board -> IO ()
showBoard = putStrLn . show
