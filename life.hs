type Board = [[Bool]]

fromBoardExp :: [String] -> Board
fromBoardExp = map (map (/= '.'))

showBool :: Bool -> Char
showBool b = if b then 'X' else '.'

valBool :: Bool -> Int
valBool b = if b then 1 else 0

showBoard :: Board -> String
showBoard = unlines . (map (map showBool))

getWidth :: Board -> Int
getWidth board = length (head board)

getHeight :: Board -> Int
getHeight = length

xIdx :: Board -> [Int]
xIdx board = take (getWidth board) [0..]

yIdx :: Board -> [Int]
yIdx board = take (getHeight board) [0..]

main :: IO ()
main = do
  contents <- getContents
  let initBoard = fromBoardExp (lines contents)
      nx = getWidth initBoard
      ny = getHeight initBoard
  putStrLn ((show nx) ++ " " ++ (show ny))
  mainLoop initBoard

mainLoop :: Board -> IO ()
mainLoop board = do
  putStr (showBoard board)
  {- only for debugging -}
  {- getChar -}
  mainLoop (nextGen board)
    
nextGen :: Board -> Board
nextGen board = map (nextGenRow board) (yIdx board)

nextGenRow :: Board -> Int -> [Bool]
nextGenRow board y = map (nextGenCell board y) (xIdx board)

nextGenCell :: Board -> Int -> Int -> Bool
nextGenCell board y x = around == 3 || (around == 2 && board!!y!!x)
  where around = calcSum board [y, iPrev y ny, iNext y ny] [x, iPrev x nx, iNext x nx]
        nx = getWidth board
        ny = getHeight board
        iPrev i ni = if 0 <= i - 1 then i - 1 else ni - 1
        iNext i ni = if i + 1 < ni then i + 1 else 0

calcSum :: Board -> [Int] -> [Int] -> Int
calcSum board ys xs = (sum . map valBool . tail) [ board!!y!!x | y <- ys, x <- xs ]
